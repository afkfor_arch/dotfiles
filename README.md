# My Dotfiles Management using GNU Stow

### First step download stow and git

```bash
sudo pacman -S git stow
```

### Next clone the git repo
```bash
git clone https://gitlab.com/afkfor_arch/dotfiles.git && cd dotfiles/
```

### Then initiate stow
```bash
stow .
```

### Done now use the system like a normal person😊
