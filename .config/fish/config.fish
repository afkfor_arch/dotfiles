if status is-interactive
    # Commands to run in interactive sessions can go here
end

# function fish_greeting
#     pokemon-colorscripts -r --no-title
# end

function multicd
    echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
end
abbr --add dotdot --regex '^\.\.+$' --function multicd

set -g fish_greeting

# Aliases
alias ls="eza --color=always --git --all --icons=always "
alias ll="eza --color=always --git --long --all --git --icons=always"
alias v="nvim"
alias dot="cd ~/dotfiles/"
alias gs="git status"
alias ga="git add ."
alias gc="git commit -m 'Random Commit'"
alias gp="git push origin main"
